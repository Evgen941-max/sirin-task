#include <stdio.h>
#include <stdlib.h>
#include <string.h>

// Define the data structures needed for the CLI
struct iface {
    char name[16];
};

struct iface default_iface = {"lo0", "gif0", "anpi2"}; // Set the default interface to lo0

struct stat {
    char iface[16];
    int packets_received;
};

struct stat stats[10]; // Store stats for up to 10 interfaces

int num_interfaces = 1; // Start with the default interface

// Implement the command functions
void start() {
    // Initialize the packet sniffing mechanism and start capturing packets from the default interface
    printf("Starting packet sniffing on interface %s\n", default_iface.name);
}

void stop() {
    // Stop capturing packets
    printf("Stopping packet sniffing\n");
}

void show(char* ip) {
    // Print the number of packets received from the given IP address
    printf("Packets received from IP address %s: %d\n", ip, 0); // Replace 0 with the actual number of packets received
}

void select_iface(char* iface) {
    // Select the given interface for packet sniffing
    printf("Selected interface: %s\n", iface);
}

void print_stats() {
    // Print the collected statistics for each interface
    for (int i = 0; i < num_interfaces; i++) {
        printf("Interface %s: packets received: %d\n", stats[i].iface, stats[i].packets_received);
    }
}

void print_usage() {
    // Print the usage information
    printf("Usage:\n");
    printf("start - start packet sniffing on the default interface\n");
    printf("stop - stop packet sniffing\n");
    printf("show [ip] count - print the number of packets received from the given IP address\n");
    printf("select iface [iface] - select the given interface for packet sniffing\n");
    printf("stat [iface] - show all collected statistics for the given interface, or all interfaces if iface is omitted\n");
    printf("--help - show usage information\n");
}

// Implement the parser
void parse_command(char* input) {
    char* command = strtok(input, " ");
    char* arg1 = strtok(NULL, " ");
    char* arg2 = strtok(NULL, " ");

    if (strcmp(command, "start") == 0) {
        start();
    } else if (strcmp(command, "stop") == 0) {
        stop();
    } else if (strcmp(command, "show") == 0) {
        show(arg1);
    } else if (strcmp(command, "select") == 0 && strcmp(arg1, "iface") == 0) {
        select_iface(arg2);
    } else if (strcmp(command, "stat") == 0) {
        if (arg1 == NULL) {
        // Show statistics for all interfaces
        print_stats();
    } else {
        // Show statistics for the specified interface
        printf("Statistics for interface %s:\n", arg1);
        for (int i = 0; i < num_interfaces; i++) {
            if (strcmp(stats[i].iface, arg1) == 0) {
            printf("Packets received: %d\n", stats[i].packets_received);
            break;
        }
    }  

    }
    } else if (strcmp(command, "--help") == 0) {
        print_usage();
    } else {
        printf("Unknown command: %s\n", command);
    }
}

// Implement the main function to read input and parse commands
int main() {
    char input[256];
    while (1) {
        printf("> ");
        fgets(input, 256, stdin);
        input[strcspn(input, "\n")] = 0; // Remove the newline character at the end of the input
        parse_command(input);
    }
    return 0;
}