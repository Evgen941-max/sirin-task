#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <arpa/inet.h>

typedef struct node {
    struct in_addr ip;
    int count;
    struct node* left;
    struct node* right;
} Node;

Node* createNode(struct in_addr ip, int count) {
    Node* newNode = (Node*) malloc(sizeof(Node));
    newNode->ip = ip;
    newNode->count = count;
    newNode->left = NULL;
    newNode->right = NULL;
    return newNode;
}

Node* insert(Node* root, struct in_addr ip, int count) {
    if (root == NULL) {
        return createNode(ip, count);
    }

    if (ntohl(ip.s_addr) < ntohl(root->ip.s_addr)) {
        root->left = insert(root->left, ip, count);
    } else if (ntohl(ip.s_addr) > ntohl(root->ip.s_addr)) {
        root->right = insert(root->right, ip, count);
    } else {
        root->count += count;
    }

    return root;
}

void inorderTraversal(Node* root) {
    if (root != NULL) {
        inorderTraversal(root->left);
        printf("%s: %d\n", inet_ntoa(root->ip), root->count);
        inorderTraversal(root->right);
    }
}

int main() {
    Node* root = NULL;

    struct in_addr ip1, ip2, ip3;
    inet_pton(AF_INET, "192.168.1.1", &ip1);
    inet_pton(AF_INET, "192.168.1.2", &ip2);
    inet_pton(AF_INET, "192.168.1.3", &ip3);

    root = insert(root, ip1, 10);
    root = insert(root, ip2, 20);
    root = insert(root, ip3, 5);

    inorderTraversal(root);

    return 0;
}