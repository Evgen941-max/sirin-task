import argparse
import socket
import struct
import sys
import time
import daemon
import lockfile

from collections import defaultdict
from select import select


class NetworkMonitor:
    def __init__(self, iface):
        self.iface = iface
        self.socket = None
        self.stats = defaultdict(int)

    def start(self):
        # create raw socket to listen for all packets on the specified interface
        self.socket = socket.socket(socket.AF_PACKET, socket.SOCK_RAW, socket.ntohs(3))
        self.socket.bind((self.iface, 0))

        print(f"Monitoring network traffic on interface {self.iface}...")

        # enter infinite loop to process packets as they arrive
        while True:
            # select on the raw socket for new data
            ready, _, _ = select([self.socket], [], [], 1)
            if ready:
                # receive packet and extract source and destination IP addresses
                packet, _ = self.socket.recvfrom(65535)
                ip_header = packet[14:34]
                src_ip, dst_ip = struct.unpack("!4s4s", ip_header)[0:2]

                # convert IP addresses to human-readable format
                src_ip = socket.inet_ntoa(src_ip)
                dst_ip = socket.inet_ntoa(dst_ip)

                # update statistics for source and destination IP addresses
                self.stats[src_ip] += 1
                self.stats[dst_ip] += 1

    def stop(self):
        if self.socket:
            self.socket.close()
            self.socket = None

        print("Stopped monitoring network traffic.")

    def show_count(self, ip):
        count = self.stats.get(ip, 0)
        print(f"Received {count} packets from {ip}.")

    def show_stats(self):
        print("Network traffic statistics:")

        for ip, count in self.stats.items():
            print(f"{ip}: {count} packets")

    def run_cli(self):
        parser = argparse.ArgumentParser()
        parser.add_argument("command", choices=["start", "stop", "show", "stat"])
        parser.add_argument("--ip", help="IP address to show count for")
        args = parser.parse_args()

        if args.command == "start":
            self.start()
        elif args.command == "stop":
            self.stop()
        elif args.command == "show":
            if not args.ip:
                print("Please provide an IP address to show count for.")
                return
            self.show_count(args.ip)
        elif args.command == "stat":
            self.show_stats()


if __name__ == '__main__':
    with daemon.DaemonContext(pidfile=lockfile.FileLock('/var/run/network_monitor.pid')):
        nm = NetworkMonitor("lo0")
        nm.run_cli()